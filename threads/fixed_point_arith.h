/*
 * fixed_point_arith.h
 *
 *  Created on: Nov 2, 2015
 *      Author: Paul-Victor Marian
 *
 *     Implements fixed point arithmetical operations, without using coprocessor instructions
 */

#ifndef FIXED_POINT_ARITH_H_
#define FIXED_POINT_ARITH_H_

#define DEBUG_MODE

/* Fixed point variable format 17.14 */
#define FIXED_POINT_Q	14
#define FIXED_POINT_F	(1 << FIXED_POINT_Q)

/**
 * Fixed point arithmetic function macros
 */
// convert integer to fixed point
#define __integer_to_fixed_point__(N)		(N * FIXED_POINT_F)
// convert fixed point to integer, round to zero
#define __fixed_point_to_integer_zero__(X)	(X / FIXED_POINT_F)
// convert fixed point to integer, round to nearest
#define __fixed_point_to_integer_near__(X)	(X >= 0 ? (X + FIXED_POINT_F / 2) / FIXED_POINT_F : \
													(X - FIXED_POINT_F / 2) / FIXED_POINT_F)

// add two fixed point numbers
#define __add_fixed_points__(X ,Y)		(X + Y)
// subtract two fixed point numbers
#define __sub_fixed_points__(X ,Y)		(X - Y)
// add fixed point with integer
#define __add_fixed_point_with_int__(X, N)		(X + N * FIXED_POINT_F)
// subtract integer from fixed point
#define __sub_int_from_fixed_point__(N, X)		(X - N * FIXED_POINT_F)
// multiply two fixed points
#define __multiply_fixed_points__(X ,Y)			(((int64_t)X) * Y / FIXED_POINT_F)
// multiply fixed point with integer
#define __multiply_fixed_point_with_int__(X, N)	(X * N)
// divide two fixed points
#define __div_fixed_points__(X, Y)				(((int64_t)X) * FIXED_POINT_F / Y)
// divide fixed point by integer
#define __div_fixed_point_by_int__(X, N)		(X / N)

#ifdef DEBUG_MODE
// fixed point pretty print
#define __print_fixed_point__(X)	printf("fixed point: %d.%d\n", \
											((X & (1 << 31)) | (X >> FIXED_POINT_Q)), \
											(X & 0x3FF))
#endif

#endif /* FIXED_POINT_ARITH_H_ */
