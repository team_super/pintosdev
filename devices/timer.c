#include "devices/timer.h"
#include <debug.h>
#include <inttypes.h>
#include <round.h>
#include <stdio.h>
#include "devices/pit.h"
#include "threads/interrupt.h"
#include "threads/synch.h"
#include "threads/thread.h"

#include "threads/fixed_point_arith.h"
  
/* See [8254] for hardware details of the 8254 timer chip. */

#if TIMER_FREQ < 19
#error 8254 timer requires TIMER_FREQ >= 19
#endif
#if TIMER_FREQ > 1000
#error TIMER_FREQ <= 1000 recommended
#endif


extern struct list sleep_list;

/* external references for 4.4BSD */
extern struct thread *idle_thread;
extern struct list ready_list;
extern struct list all_list;
extern struct list mlfq_list;
extern int load_avg;

/* Number of timer ticks since OS booted. */
static int64_t ticks;

/* Number of loops per timer tick.
   Initialized by timer_calibrate(). */
static unsigned loops_per_tick;

static intr_handler_func timer_interrupt;
static bool too_many_loops (unsigned loops);
static void busy_wait (int64_t loops);
static void real_time_sleep (int64_t num, int32_t denom);
static void real_time_delay (int64_t num, int32_t denom);

/* Sets up the timer to interrupt TIMER_FREQ times per second,
   and registers the corresponding interrupt. */
void
timer_init (void) 
{
  pit_configure_channel (0, 2, TIMER_FREQ);
  intr_register_ext (0x20, timer_interrupt, "8254 Timer");
}

/* Calibrates loops_per_tick, used to implement brief delays. */
void
timer_calibrate (void) 
{
  unsigned high_bit, test_bit;

  ASSERT (intr_get_level () == INTR_ON);
  printf ("Calibrating timer...  ");

  /* Approximate loops_per_tick as the largest power-of-two
     still less than one timer tick. */
  loops_per_tick = 1u << 10;
  while (!too_many_loops (loops_per_tick << 1)) 
    {
      loops_per_tick <<= 1;
      ASSERT (loops_per_tick != 0);
    }

  /* Refine the next 8 bits of loops_per_tick. */
  high_bit = loops_per_tick;
  for (test_bit = high_bit >> 1; test_bit != high_bit >> 10; test_bit >>= 1)
    if (!too_many_loops (high_bit | test_bit))
      loops_per_tick |= test_bit;

  printf ("%'"PRIu64" loops/s.\n", (uint64_t) loops_per_tick * TIMER_FREQ);
}

/* Returns the number of timer ticks since the OS booted. */
int64_t
timer_ticks (void) 
{
  enum intr_level old_level = intr_disable ();
  int64_t t = ticks;
  intr_set_level (old_level);
  return t;
}

/* Returns the number of timer ticks elapsed since THEN, which
   should be a value once returned by timer_ticks(). */
int64_t
timer_elapsed (int64_t then) 
{
  return timer_ticks () - then;
}

/* Sleeps for approximately TICKS timer ticks.  Interrupts must
   be turned on. */
void
timer_sleep (int64_t ticks) 
{
  int64_t start = timer_ticks ();

  ASSERT (intr_get_level () == INTR_ON);

  if (ticks >= 0)
  {
	thread_current()->sleep_ticks = start + ticks;
    list_insert_ordered(&sleep_list, &thread_current()->elem,
		  (list_less_func *)&sleep_order, NULL);
    enum intr_level old_level = intr_disable();
    thread_block();
    intr_set_level(old_level);
  }
}

/* Sleeps for approximately MS milliseconds.  Interrupts must be
   turned on. */
void
timer_msleep (int64_t ms) 
{
  real_time_sleep (ms, 1000);
}

/* Sleeps for approximately US microseconds.  Interrupts must be
   turned on. */
void
timer_usleep (int64_t us) 
{
  real_time_sleep (us, 1000 * 1000);
}

/* Sleeps for approximately NS nanoseconds.  Interrupts must be
   turned on. */
void
timer_nsleep (int64_t ns) 
{
  real_time_sleep (ns, 1000 * 1000 * 1000);
}

/* Busy-waits for approximately MS milliseconds.  Interrupts need
   not be turned on.

   Busy waiting wastes CPU cycles, and busy waiting with
   interrupts off for the interval between timer ticks or longer
   will cause timer ticks to be lost.  Thus, use timer_msleep()
   instead if interrupts are enabled. */
void
timer_mdelay (int64_t ms) 
{
  real_time_delay (ms, 1000);
}

/* Sleeps for approximately US microseconds.  Interrupts need not
   be turned on.

   Busy waiting wastes CPU cycles, and busy waiting with
   interrupts off for the interval between timer ticks or longer
   will cause timer ticks to be lost.  Thus, use timer_usleep()
   instead if interrupts are enabled. */
void
timer_udelay (int64_t us) 
{
  real_time_delay (us, 1000 * 1000);
}

/* Sleeps execution for approximately NS nanoseconds.  Interrupts
   need not be turned on.

   Busy waiting wastes CPU cycles, and busy waiting with
   interrupts off for the interval between timer ticks or longer
   will cause timer ticks to be lost.  Thus, use timer_nsleep()
   instead if interrupts are enabled.*/
void
timer_ndelay (int64_t ns) 
{
  real_time_delay (ns, 1000 * 1000 * 1000);
}

/* Prints timer statistics. */
void
timer_print_stats (void) 
{
  printf ("Timer: %"PRId64" ticks\n", timer_ticks ());
}

/* Timer interrupt handler. */
static void
timer_interrupt (struct intr_frame *args UNUSED)
{
  ticks++;
  thread_tick ();

  /* wake up apropriate sleeping threads */
  if ( !list_empty(&sleep_list) )
  {
    struct list_elem *el;
    struct thread *th;
    enum intr_level old_level = intr_disable();
    for (el = list_begin(&sleep_list); el != list_end(&sleep_list); el = list_next(el))
    {
	  th = list_entry(el, struct thread, elem);
	  if (ticks >= th->sleep_ticks)  // sleep time passed
	  {
		list_remove(el); // remove from sleeping list
	    thread_unblock(th); // and add thread to ready list
	    if ( !list_empty(&sleep_list) )
	      el = list_begin(&sleep_list);
	    else
	    {
	      intr_set_level(old_level);
	      return;
	    }
	  }
    }
    intr_set_level(old_level);
  }

  /* check if 4.4BSD scheduler selected */
  if (true == thread_mlfqs)
  {
	struct thread *t = thread_current();

	/* increment recent_cpu of the running thread */
	if (t != idle_thread)
	  t->recent_cpu = __add_fixed_point_with_int__(t->recent_cpu, 1);

	// once per second execution
	if (TIMER_FREQ == ticks)
	{
	  /* compute load average */
	  int nr_rdy_threads = list_size(&ready_list);
	  nr_rdy_threads--; // don't take into account the idle thread
	  int math_var1 = __div_fixed_point_by_int__( __integer_to_fixed_point__(59), 60);
	  int math_var2 = __div_fixed_point_by_int__( __integer_to_fixed_point__(nr_rdy_threads), 60);
	  math_var1 = __multiply_fixed_points__(math_var1, load_avg);
	  load_avg = __add_fixed_points__(math_var1, math_var2);

	  // recalculate recent_cpu for all threads
	  struct list_elem *el;
	  for (el = list_begin(&all_list); el != list_end(&all_list); el = list_next(el))
	  {
		t = list_entry(el, struct thread, allelem);
		if (t != idle_thread)
		{
		  /* calculate the new recent_cpu value */
		  int temp = __multiply_fixed_point_with_int__(load_avg, 2);
		  temp = __div_fixed_points__(temp, __add_fixed_point_with_int__(temp, 1));
		  temp = __multiply_fixed_points__(temp, t->recent_cpu);
		  t->recent_cpu = __add_fixed_point_with_int__(temp, t->nice);

		  /* calculate the new priority */
		  temp = __div_fixed_point_by_int__(t->recent_cpu, 4);
		  temp = __sub_fixed_points__(PRI_MAX_FP, temp);
		  t->priority = __fixed_point_to_integer_near__(__sub_fixed_points__(temp, __integer_to_fixed_point__(t->nice * 2)));
		  if(t->priority < PRI_MIN)
		    t->priority = PRI_MIN;
		  if(t->priority > PRI_MAX)
			t->priority = PRI_MAX;
		}
	  }
	}
	else if (TIMER_CALC_FREQ == ticks)
	{
	  struct list_elem *el;
	  /* iterate through all threads that ran since the last time this code ran */
	  for (el = list_begin(&mlfq_list); el != list_end(&mlfq_list); el = list_next(el))
	  {
		t = list_entry(el, struct thread, mlfqelem);
		/* calculate the new priority */
		int temp = __div_fixed_point_by_int__(t->recent_cpu, 4);
		temp = __sub_fixed_points__(PRI_MAX_FP, temp);
	    t->priority = __fixed_point_to_integer_near__(__sub_fixed_points__(temp, __integer_to_fixed_point__(t->nice * 2)));
		if(t->priority < PRI_MIN)
		  t->priority = PRI_MIN;
		if(t->priority > PRI_MAX)
		  t->priority = PRI_MAX;
	  }
	  list_init(&mlfq_list);
	}
  }
}

/* Returns true if LOOPS iterations waits for more than one timer
   tick, otherwise false. */
static bool
too_many_loops (unsigned loops) 
{
  /* Wait for a timer tick. */
  int64_t start = ticks;
  while (ticks == start)
    barrier ();

  /* Run LOOPS loops. */
  start = ticks;
  busy_wait (loops);

  /* If the tick count changed, we iterated too long. */
  barrier ();
  return start != ticks;
}

/* Iterates through a simple loop LOOPS times, for implementing
   brief delays.

   Marked NO_INLINE because code alignment can significantly
   affect timings, so that if this function was inlined
   differently in different places the results would be difficult
   to predict. */
static void NO_INLINE
busy_wait (int64_t loops) 
{
  while (loops-- > 0)
    barrier ();
}

/* Sleep for approximately NUM/DENOM seconds. */
static void
real_time_sleep (int64_t num, int32_t denom) 
{
  /* Convert NUM/DENOM seconds into timer ticks, rounding down.
          
        (NUM / DENOM) s          
     ---------------------- = NUM * TIMER_FREQ / DENOM ticks. 
     1 s / TIMER_FREQ ticks
  */
  int64_t ticks = num * TIMER_FREQ / denom;

  ASSERT (intr_get_level () == INTR_ON);
  if (ticks > 0)
    {
      /* We're waiting for at least one full timer tick.  Use
         timer_sleep() because it will yield the CPU to other
         processes. */                
      timer_sleep (ticks); 
    }
  else 
    {
      /* Otherwise, use a busy-wait loop for more accurate
         sub-tick timing. */
      real_time_delay (num, denom); 
    }
}

/* Busy-wait for approximately NUM/DENOM seconds. */
static void
real_time_delay (int64_t num, int32_t denom)
{
  /* Scale the numerator and denominator down by 1000 to avoid
     the possibility of overflow. */
  ASSERT (denom % 1000 == 0);
  busy_wait (loops_per_tick * num / 1000 * TIMER_FREQ / (denom / 1000)); 
}

/* Function used when inserting a new element in the sleeping list,
 * maintaining it ordered */
bool sleep_order(const struct list_elem *to_insert, const struct list_elem *from_list, void *aux UNUSED)
{
  struct thread *t_to_insert = list_entry (to_insert, struct thread, elem);
  struct thread *t_from_list = list_entry (from_list, struct thread, elem);
  if (t_to_insert->sleep_ticks <= t_from_list->sleep_ticks)
	return true;
  return false;
}
